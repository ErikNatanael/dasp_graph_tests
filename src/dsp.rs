use dasp_graph::{Buffer, Input, Node};
use std::f64::consts::PI;

#[derive(Copy, Clone)]
pub struct Sine {
    pub sample_rate: f64,
    pub phase: f64,
    pub freq: f64,
    pub amp: f64,
    pub add: f64,
}

impl Sine {
    pub fn new() -> Self {
        Sine {
            phase: 0.0,
            freq: 220.0,
            amp: 0.0,
            add: 0.0,
            sample_rate: 44100.0,
        }
    }

    pub fn from(freq: f64, amp: f64, add: f64, phase: f64, sample_rate: f64) -> Self {
        Sine {
            phase,
            freq,
            amp,
            add,
            sample_rate,
        }
    }

    pub fn set_range(&mut self, min: f64, max: f64) {
        self.amp = ((max - min) / 2.0).abs();
        self.add = (max + min) / 2.0;
    }

    pub fn tick(&mut self) -> f64 {
        let sine_amp = (2.0 * PI * self.phase).sin();
        self.phase += self.freq / self.sample_rate;
        while self.phase >= 1.0 {
            self.phase -= 1.0;
        }
        (sine_amp * self.amp) + self.add
    }
}
impl Node for Sine {
    fn process(&mut self, inputs: &[Input], output: &mut [Buffer]) {
        for i in 0..output[0].len() {
            output[0][i] = self.tick() as f32;
        }
    }
}

pub struct ModulatableSine {
    pub sine: Sine,
}

impl ModulatableSine {
    pub fn new() -> Self {
        ModulatableSine { sine: Sine::new() }
    }

    pub fn from(freq: f64, amp: f64, add: f64, phase: f64, sample_rate: f64) -> Self {
        ModulatableSine {
            sine: Sine::from(phase, freq, amp, add, sample_rate),
        }
    }
}
impl Node for ModulatableSine {
    fn process(&mut self, inputs: &[Input], output: &mut [Buffer]) {
        // Inputs:
        // 0 freq
        // 1 amp
        // 2 add
        for i in 0..output[0].len() {
            if inputs.len() > 0 {
                self.sine.freq = inputs[0].buffers()[0][i].into();
                // print!("freq: {} ", self.sine.freq);
            }
            if inputs.len() > 1 {
                self.sine.amp = inputs[1].buffers()[0][i].into();
                // print!("amp: {} ", self.sine.amp);
            }
            if inputs.len() > 2 {
                self.sine.add = inputs[2].buffers()[0][i].into();
            }

            output[0][i] = self.sine.tick() as f32;
            // println!("out: {}", output[0][i]);
        }
    }
}

pub struct NodeWithInputs;

impl Node for NodeWithInputs {
    fn process(&mut self, inputs: &[Input], output: &mut [Buffer]) {
        for inp in inputs.iter() {
            let in_bufs = inp.buffers();
            for in_buf in in_bufs.iter() {
                print!("{}, ", in_buf[0]);
            }
        }
        println!();

        for out in output.iter_mut() {
            out.silence();
        }
    }
}

pub struct ConstNode(pub f32);

impl Node for ConstNode {
    fn process(&mut self, inputs: &[Input], output: &mut [Buffer]) {
        for i in 0..output[0].len() {
            output[0][i] = self.0;
        }
    }
}
