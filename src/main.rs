use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use dasp::{signal, Sample, Signal};
use petgraph::graph::{EdgeIndex, NodeIndex};
use petgraph::stable_graph::{StableDiGraph, StableGraph};
use petgraph::Directed;
use rand::prelude::*;
use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use rand::{Rng, SeedableRng};

use dasp_graph::{BoxedNodeSend, NodeData, Processor};
use std::sync::mpsc;

mod dsp;
use dsp::*;

// Chose a type of graph for audio processing.
type MyGraph = StableGraph<NodeData<BoxedNodeSend>, (), Directed, u32>;
// Create a short-hand for our processor type.
type MyProcessor = Processor<MyGraph>;

fn main() -> anyhow::Result<()> {
    let host =
        cpal::host_from_id(cpal::available_hosts()
            .into_iter()
            .find(|id| *id == cpal::HostId::Jack)
            .expect(
                "make sure --features jack is specified. only works on OSes where jack is available",
            )).expect("jack host unavailable");

    let device = host
        .default_output_device()
        .expect("failed to find output device");
    println!("Output device: {}", device.name()?);

    let config = device.default_output_config().unwrap();
    println!("Default output config: {:?}", config);

    match config.sample_format() {
        cpal::SampleFormat::F32 => run::<f32>(&device, &config.into()),
        cpal::SampleFormat::I16 => run::<i16>(&device, &config.into()),
        cpal::SampleFormat::U16 => run::<u16>(&device, &config.into()),
    }
}

fn run<T>(device: &cpal::Device, config: &cpal::StreamConfig) -> Result<(), anyhow::Error>
where
    T: cpal::Sample,
{
    let freqs = vec![220.0, 330.0, 440.0, 550.0, 660.0];
    let mut sines = vec![];
    for freq in &freqs {
        let hz = signal::rate(config.sample_rate.0 as f64).const_hz(*freq);
        let sine = hz.sine().mul_amp(signal::gen(|| 0.1));
        sines.push(sine);
    }

    // Audio graph

    let max_nodes = 1024;
    let max_edges = 1024;
    let mut g = MyGraph::with_capacity(max_nodes, max_edges);
    let mut p = MyProcessor::with_capacity(max_nodes);

    let mut sine_indices = vec![];

    let sr = config.sample_rate.0 as f64;

    // Add the Signal sines to the graph
    // This doesn't work. Why?
    /*for sine in sines {
        let boxed_sine: Box<dyn Signal<Frame = f64>> = Box::new(sine);
        let node_sine = BoxedNodeSend::new(boxed_sine);
        // Add a node to the graph and receive its index in the graph
        let index = g.add_node(node_sine);
        sine_indices.push(index);
    }*/

    // The stateless Sum node will sum each of its inputs into its output
    let sum_node_index = g.add_node(NodeData::new1(BoxedNodeSend::new(dasp_graph::node::Sum)));

    // Add sines to the graph
    for freq in &freqs {
        let sine = dsp::Sine::from(*freq, 0.1, 0.0, 0.0, sr);
        let node_sine = BoxedNodeSend::new(sine);
        // Create the NodeData which is just the node and a buffer for its output
        let node_data_sine = NodeData::new1(node_sine);
        // Add a node to the graph and receive its index in the graph
        let index = g.add_node(node_data_sine);
        sine_indices.push(index);
        // Connect the sine wave to the summation node
        // g.add_edge(index, sum_node_index, ());
    }

    // Test node inputs
    let test_node = NodeData::new1(BoxedNodeSend::new(NodeWithInputs));
    let test_node_i = g.add_node(test_node);
    let in1 = NodeData::new1(BoxedNodeSend::new(ConstNode(1.0)));
    let in1_i = g.add_node(in1);
    g.add_edge(in1_i, test_node_i, ());
    let in2 = NodeData::new1(BoxedNodeSend::new(ConstNode(2.0)));
    let in2_i = g.add_node(in2);
    g.add_edge(in2_i, test_node_i, ());
    let in3 = NodeData::new1(BoxedNodeSend::new(ConstNode(3.0)));
    let in3_i = g.add_node(in3);
    g.add_edge(in3_i, test_node_i, ());

    p.process(&mut g, test_node_i);

    // Replace the nodes providing the values
    *(g.node_weight_mut(in1_i).unwrap()) = NodeData::new1(BoxedNodeSend::new(ConstNode(100.0)));
    *(g.node_weight_mut(in2_i).unwrap()) = NodeData::new1(BoxedNodeSend::new(ConstNode(200.0)));
    *(g.node_weight_mut(in3_i).unwrap()) = NodeData::new1(BoxedNodeSend::new(ConstNode(300.0)));

    p.process(&mut g, test_node_i);

    // Add modulated sine
    let mod_sine = ModulatableSine::from(300.0, 0.1, 0.0, 0.0, sr);
    let mod_sine_i = g.add_node(NodeData::new1(BoxedNodeSend::new(mod_sine)));
    let freq_mod = Sine::from(0.5, 50.0, 200.0, 0.0, sr);
    let freq_mod_i = g.add_node(NodeData::new1(BoxedNodeSend::new(freq_mod)));
    let amp_mod = Sine::from(1.0, 0.1, 0.2, 0.0, sr);
    let amp_mod_i = g.add_node(NodeData::new1(BoxedNodeSend::new(amp_mod)));

    // Add edges in reverse order
    g.add_edge(amp_mod_i, mod_sine_i, ());
    g.add_edge(freq_mod_i, mod_sine_i, ());

    // Connect the oscillator to the output node
    g.add_edge(mod_sine_i, sum_node_index, ());

    // Add a modulated sine through the abstraction

    let mut sine_abs = ModSineAbstraction::new(sr, &mut g);
    sine_abs.params(500.0, 0.25, 0.0, &mut g);
    sine_abs.connect_to_node(sum_node_index, &mut g);

    let mut sample_index = dasp_graph::Buffer::LEN;
    let mut rng = SmallRng::from_entropy();

    // Create and run the stream.
    let err_fn = |err| eprintln!("an error occurred on stream: {}", err);
    let channels = config.channels as usize;
    let stream = device.build_output_stream(
        config,
        move |data: &mut [T], _: &cpal::OutputCallbackInfo| {
            // Randomly change the modulation node sometimes
            if rng.gen::<f32>() > 0.99 {
                *(g.node_weight_mut(freq_mod_i).unwrap()) = match (0..3).choose(&mut rng).unwrap() {
                    0 => NodeData::new1(BoxedNodeSend::new(Sine::from(0.5, 50.0, 200.0, 0.0, sr))),
                    1 => NodeData::new1(BoxedNodeSend::new(Sine::from(0.5, 100.0, 400.0, 0.0, sr))),
                    _ => {
                        let freq = [136.0, 180.0, 220.0, 280.0].choose(&mut rng).unwrap();
                        NodeData::new1(BoxedNodeSend::new(ConstNode(*freq)))
                    }
                }
            }
            // write_data_signal(data, channels, &mut summed_sines)
            write_data_graph(
                data,
                channels,
                &mut g,
                &mut p,
                sum_node_index,
                &mut sample_index,
            )
        },
        err_fn,
    )?;
    stream.play()?;

    // Wait for a while.
    std::thread::sleep(std::time::Duration::from_millis(20000));
    stream.pause()?;

    Ok(())
}

fn write_data_signal<T>(
    output: &mut [T],
    channels: usize,
    signal: &mut dyn Signal<Frame = [f64; 1]>,
) where
    T: cpal::Sample,
{
    for frame in output.chunks_mut(channels) {
        let sample = signal.next();
        let value: T = cpal::Sample::from::<f32>(&(sample[0] as f32));
        for sample in frame.iter_mut() {
            *sample = value;
        }
    }
}

fn write_data_graph<T>(
    output: &mut [T],
    channels: usize,
    graph: &mut MyGraph,
    processor: &mut MyProcessor,
    end_node: NodeIndex,
    sample_index: &mut usize,
) where
    T: cpal::Sample,
{
    for frame in output.chunks_mut(channels) {
        if *sample_index >= dasp_graph::Buffer::LEN {
            // Process the graph and fill the output buffer again
            processor.process(graph, end_node);
            *sample_index = 0;
        }
        let output_buffer = &graph[end_node].buffers[0];
        let sample = output_buffer[*sample_index];
        *sample_index += 1;
        let value: T = cpal::Sample::from::<f32>(&(sample));
        for sample in frame.iter_mut() {
            *sample = value;
        }
    }
}

/// Takes care of adding and keeping track of the necessary nodes in a graph for a specific "UGen"
struct ModSineAbstraction {
    pub osc_i: NodeIndex,
    // Summation nodes for the inputs:
    pub freq_i: NodeIndex,
    pub amp_i: NodeIndex,
    pub add_i: NodeIndex,

    // Optional constant nodes to set an initial value to the parameters
    pub freq_const_i: Option<NodeIndex>,
    pub amp_const_i: Option<NodeIndex>,
    pub add_const_i: Option<NodeIndex>,
}

impl ModSineAbstraction {
    pub fn new(sample_rate: f64, graph: &mut MyGraph) -> Self {
        let sine = ModulatableSine::from(300.0, 0.0, 0.0, 0.0, sample_rate);
        let osc_i = graph.add_node(NodeData::new1(BoxedNodeSend::new(sine)));
        let freq_i = graph.add_node(NodeData::new1(BoxedNodeSend::new(dasp_graph::node::Sum)));
        let amp_i = graph.add_node(NodeData::new1(BoxedNodeSend::new(dasp_graph::node::Sum)));
        let add_i = graph.add_node(NodeData::new1(BoxedNodeSend::new(dasp_graph::node::Sum)));

        // Add edges in reverse order
        graph.add_edge(add_i, osc_i, ());
        graph.add_edge(amp_i, osc_i, ());
        graph.add_edge(freq_i, osc_i, ());

        ModSineAbstraction {
            osc_i,
            freq_i,
            amp_i,
            add_i,
            freq_const_i: None,
            amp_const_i: None,
            add_const_i: None,
        }
    }

    pub fn connect_to_node(&self, node: NodeIndex, graph: &mut MyGraph) -> EdgeIndex {
        graph.add_edge(self.osc_i, node, ())
    }

    pub fn params(&mut self, freq: f32, amp: f32, add: f32, graph: &mut MyGraph) {
        let freq_const_i = graph.add_node(NodeData::new1(BoxedNodeSend::new(ConstNode(freq))));
        graph.add_edge(freq_const_i, self.freq_i, ());
        self.freq_const_i = Some(freq_const_i);
        let amp_const_i = graph.add_node(NodeData::new1(BoxedNodeSend::new(ConstNode(amp))));
        graph.add_edge(amp_const_i, self.amp_i, ());
        self.amp_const_i = Some(amp_const_i);
        let add_const_i = graph.add_node(NodeData::new1(BoxedNodeSend::new(ConstNode(add))));
        graph.add_edge(add_const_i, self.add_i, ());
        self.add_const_i = Some(add_const_i);
    }

    pub fn remove_const_params(&mut self, graph: &mut MyGraph) {
        // TODO: Save the edges so that I won't have to search for them

        if let Some(freq_const_i) = self.freq_const_i {
            if let Some(edge) = graph.find_edge(freq_const_i, self.freq_i) {
                graph.remove_edge(edge);
            }
        }
        if let Some(amp_const_i) = self.amp_const_i {
            if let Some(edge) = graph.find_edge(amp_const_i, self.amp_i) {
                graph.remove_edge(edge);
            }
        }
        if let Some(add_const_i) = self.add_const_i {
            if let Some(edge) = graph.find_edge(add_const_i, self.add_i) {
                graph.remove_edge(edge);
            }
        }
    }
}
